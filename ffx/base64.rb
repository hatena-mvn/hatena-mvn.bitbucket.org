require 'fileutils'

files = Dir::entries("img")

# ignore files
files.delete(".")
files.delete("..")
files.delete(".DS_Store")

system("echo '- var images={};' > base64.jade")

files.each{|file|
	name = file.split(".")[0]
	system("convert img/#{file} png_img/#{name}.png")
	system("convert -geometry 150x150 png_img/#{name}.png png_img/#{name}.png")
	system("s=`base64 png_img/#{name}.png`; echo \"- images['#{name}'] = '${s}';\" >> base64.jade")
}